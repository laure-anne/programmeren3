﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebWinkel.Controllers
   
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Mik Mak webwinkel";
            return View(); 
        }

        public ActionResult AdminIndex()
        {
            ViewBag.Title = "Mik Mak beheer Pagina";
            ViewBag.Feedback = "Database Webwinkel is al gemaakt.";
            Models.Dal Dal = new Models.Dal();
            if (!Dal.Database.Exists())
            {
                try
                {
                    Dal.Database.Create();
                    ViewBag.Feedback = "Database Webwinkel is gemaakt!";
                }
                catch (Exception e)
                {
                    ViewBag.Feedback = e.Message;
                }
            }

            return View();
        }
    }
}