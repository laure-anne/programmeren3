﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebWinkel.Models;

namespace WebWinkel.Controllers

{
    public class UnitBaseController : Controller
    {
        public ActionResult Editing()
        {
            return View(); 
        }

        [HttpPost]
        public ActionResult Insert(string UnitBaseCode, string UnitBaseName,
      string UnitBaseDescription)
        {
            Models.Dal dal = new Models.Dal();
            Models.UnitBase unitBase = new Models.UnitBase();
            unitBase.Code = UnitBaseCode;
            unitBase.Name = UnitBaseName;
            unitBase.Description = UnitBaseDescription;
            if (TryValidateModel(unitBase))
            {
                dal.dBSetUnitBase.Add(unitBase);
                dal.SaveChanges();
            }
            return View("Inserting"); 
        }

        public ActionResult Inserting()
        {
            return View(); 
        }

    
        public ActionResult InsertingCancel()
        {
            Dal dal = new Dal();
            return View("Editing"); 
        }

        public ActionResult ReadingAll()
        {
            Models.Dal dal = new Models.Dal();
            return PartialView(dal.dBSetUnitBase); 
        }



    }
}